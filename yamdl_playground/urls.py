"""
URL configuration for yamdl_playground project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from .core import views
from django.apps import apps

urlpatterns = [
    path("search/", views.search),
    path("content/<slug:slug>/", views.content),
    path("cached-content/<slug:slug>/", views.cached_content)
]

if apps.is_installed("django.contrib.admin"):
    from django.contrib import admin

    urlpatterns.append(path("admin/", admin.site.urls))

if apps.is_installed("debug_toolbar"):
    urlpatterns.append(path("__debug__/", include("debug_toolbar.urls")))
