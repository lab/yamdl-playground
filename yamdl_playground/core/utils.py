from yamdl.loader import ModelLoader

class CustomYamdlLoader(ModelLoader):
    def load_fixture(self, model_class, data, file_path):
        if file_path.suffix not in self.EXT_YAML:
            data["slug"] = file_path.stem

        data["file_path"] = file_path

        super().load_fixture(model_class, data, file_path)


def show_debug_toolbar(request):
    return True
