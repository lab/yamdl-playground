from django.apps import AppConfig
from django.db import connections
from django.db.models.signals import post_save
import warnings
from django.utils.html import strip_tags
from django.core.management import call_command
from django.core.files.storage import storages
import shutil


class CoreConfig(AppConfig):
    name = "yamdl_playground.core"

    loaded = False

    def ready(self):
        from .models import Page

        if not self.loaded:
            # Clear storage to ensure files are always fresh
            shutil.rmtree(storages["yamdl"].location, ignore_errors=True)

            connection = connections["yamdl"]

            with warnings.catch_warnings():
                # Django doesn't like running DB queries during app initialization
                warnings.filterwarnings("ignore", module="django.db", category=RuntimeWarning)

                # Run migrations, if there are any
                call_command("migrate", verbosity=0, no_input=True)

                with connection.cursor() as cursor:
                    cursor.execute("CREATE VIRTUAL TABLE search_index USING fts5(body, content='');")

            post_save.connect(self.post_save, sender=Page)
            self.loaded = True

    def post_save(self, sender, instance, created, **kwargs):
        if not created:
            return

        with connections["yamdl"].cursor() as cursor:
            cursor.execute("INSERT INTO search_index(body, rowid) VALUES (%s, %s)", [strip_tags(instance.content), instance.pk])

        with connections["yamdl"].cursor() as cursor:
            cursor.execute("PRAGMA optimise;")
