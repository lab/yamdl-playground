from .models import Page
from django.http import HttpResponse
from django.db import connections
from django.shortcuts import get_object_or_404, render
from django.views.decorators.cache import cache_page
from django.utils.safestring import mark_safe


def search(request):
    with connections["yamdl"].cursor() as cursor:
        cursor.execute("SELECT rowid FROM search_index WHERE search_index = %s;", ["content"])
        row = cursor.fetchone()
    pages = Page.objects.filter(id__in=row)
    return HttpResponse(str(pages), content_type="text/plain")


def content(request, slug):
    page = get_object_or_404(Page, slug=slug)

    return render(request, "content.html", {
        "page": page,
        "content": page.render_content({"request": request}) if page.runtime_render else mark_safe(page.content)
    })

cached_content = cache_page(600, cache="mem")(content)
