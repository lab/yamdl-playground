from django.contrib import admin
from .models import Page

class PageAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "slug"]
    list_display_links = ["id", "title"]
    sortable_by = ["title"]
    ordering = ["title"]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [field.name for field in (obj._meta.fields + obj._meta.many_to_many)]

    @admin.options.csrf_protect_m
    def changeform_view(self, *args, **kwargs):
        """
        Override to prevent issues with `db_for_write`
        """
        return self._changeform_view(*args, **kwargs)

admin.site.register(Page, PageAdmin)
