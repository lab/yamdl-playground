from django_jinja import library

from jinja2_simple_tags import ContainerTag

@library.extension
class MyTag(ContainerTag):
    tags = {"mytag"}

    def render(self, caller=None):
        return "foo{}bar".format(caller())
