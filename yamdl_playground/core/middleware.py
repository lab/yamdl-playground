from django.contrib.auth.models import AnonymousUser

class FakeUser(AnonymousUser):
    is_authenticated = True
    is_anonymous = False
    is_staff = True
    is_active = True
    is_superuser = True
    username = "Fake user"

    def has_module_perms(self, module):
        # Don't allow access to the 'auth' app
        # Also stops it showing up in the admin
        return module not in {"auth"}

    def has_perm(self, perm, obj=None):
        return True

class FakeAuthenticationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # HACK: Inject our user in to user caches
        request._cached_user = request._acached_user = FakeUser()
        return self.get_response(request)
