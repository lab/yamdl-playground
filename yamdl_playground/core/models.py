from django.db import models
import markdown
from django.utils.functional import cached_property
from django.core.files.storage import storages
from django.core.files import File
from pathlib import Path
import os.path
from django.template import engines
from django_jinja.backend import Origin
from django.conf import settings

class Tag(models.Model):
    __yamdl__ = True

    slug = models.SlugField(max_length=128, primary_key=True)
    file_path = models.CharField(max_length=255)

    content = models.TextField()

def get_page_upload_to(instance, filename):
    return f"{instance._meta.verbose_name}/{instance.slug}/{os.path.basename(filename)}"

class Page(models.Model):
    __yamdl__ = True

    _template_cache = {}

    title = models.CharField(max_length=255)

    raw_content = models.TextField()
    runtime_render = models.BooleanField(default=False)
    content = models.TextField()
    toc = models.JSONField()
    slug = models.CharField(max_length=128, unique=True, db_index=True, default=None, null=True)
    file_path = models.CharField(max_length=255)

    media = models.FileField(null=True, default=None, storage=storages["yamdl"], upload_to=get_page_upload_to)

    tags = models.ManyToManyField(Tag)

    @classmethod
    def from_yaml(cls, **data):
        tags = data.pop("tags", None)

        md = markdown.Markdown(extensions=["toc"])

        content = data.pop("content")
        data["raw_content"] = content

        data["content"] = md.convert(content)

        data["toc"] = {
            "html": md.toc,
            "tokens": md.toc_tokens
        }

        if data.get("media"):
            source_dir = Path(data["file_path"]).parent

            data["media"] = File((source_dir / data["media"]).open("rb"))

        instance = cls(**data)

        if not instance.runtime_render:
            instance.content = instance.render_content()

        instance.save()

        if tags:
            instance.tags.set(tags)

        return instance

    @cached_property
    def content_template(self):
        if cached_template := self._template_cache.get(self.slug):
            return cached_template

        template_name = Path(self.file_path).relative_to(settings.YAMDL_DIRECTORIES[0])

        template = engines["jinja2"].from_string(self.content)
        template.origin = Origin(name=self.file_path, template_name=template_name)
        template.name = template.template.name = template_name

        if not self.runtime_render:
            self._template_cache[self.slug] = template

        return template

    def get_context(self):
        return {
            "page": self
        }

    def render_content(self, extra_context=None):
        if extra_context is None:
            extra_context = {}

        return self.content_template.render({**self.get_context(), **extra_context})
